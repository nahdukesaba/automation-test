Feature: Amazon login

  Scenario: I login with invalid email
    Given I nagivate to "https://www.amazon.com/"
    When I click on element having xpath "//*[@id='nav-link-accountList']"
    And I should see page title as "Amazon Sign-In"
    And I enter "nahduskesaba@gmail.com" into input field having id "ap_email"
    And I click on element having id "continue"
    Then element having xpath "//*[@id='auth-error-message-box']/div/div/ul/li/span" should have text as "Your password is incorrect"

  Scenario: I login with invalid password
    Given I nagivate to "https://www.amazon.com/"
    When I click on element having xpath "//*[@id='nav-link-accountList']"
    And I should see page title as "Amazon Sign-In"
    And I enter "jenpresly18.jps@gmail.com" into input field having id "ap_email"
    And I wait for 3 sec
    And I click on element having id "continue"
    And element having id "ap_password" should be present
    And I enter "kesaba" into input field having id "ap_password"
    And I wait for 3 sec
    And I click on element having id "signInSubmit"
    And I wait for 3 sec
    Then element having xpath "//*[@id='auth-error-message-box']/div/div/ul/li/span" should have text as "We cannot find an account with that email address"

  Scenario: I login with valid credential
    Given I nagivate to "https://www.amazon.com/"
    When I click on element having xpath "//*[@id='nav-link-accountList']"
    And I should see page title as "Amazon Sign-In"
    And I enter "jenpresly18.jps@gmail.com" into input field having id "ap_email"
    And I wait for 3 sec
    And I click on element having id "continue"
    And element having id "ap_password" should be present
    And I enter "admin123" into input field having id "ap_password"
    And I wait for 3 sec
    And I click on element having id "signInSubmit"
    And I wait for 3 sec
    And I click on element having id "continue"
    And I enter "740171" into input field having xpath "//*[@id='cvf-page-content']/div/div/div[1]/form/div[2]/input"
    And I wait for 20 sec
    And I click on element having class "a-button-input"
    And I wait for 20 sec
    Then element having xpath "//*[@id='nav-link-accountList']/span[1]" should have text as "Hello, Jen"