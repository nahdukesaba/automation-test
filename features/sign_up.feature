Feature: Amazon sign up

  Scenario: Submit empty data
    Given I navigate to "https://www.amazon.com/"
    When I click on element having xpath "//*[@id='nav-link-accountList']"
    And I click on element having xpath "//*[@id='createAccountSubmit']"
    And I click on element having xpath "//*[@id='continue']"
    Then element having xpath "//*[@id='auth-customerName-missing-alert']/div/div" should be present
    And element having xpath "//*[@id='auth-customerName-missing-alert']/div/div" should have text as "Enter your name"
    And element having xpath "//*[@id='auth-email-missing-alert']/div/div" should be present
    And element having xpath "//*[@id='auth-email-missing-alert']/div/div" should have text as "Enter your email"
    And element having xpath "//*[@id='auth-password-missing-alert']/div/div" should be present
    And element having xpath "//*[@id='auth-password-missing-alert']/div/div" should have text as "Enter your password"
    
  Scenario: Submit wrong email
    Given I navigate to "https://www.amazon.com/"
    When I click on element having xpath "//*[@id='nav-link-accountList']"
    And I click on element having xpath "//*[@id='createAccountSubmit']"
    And I enter "name" into input field having xpath "//*[@id='ap_customer_name']"
    And I enter "wrongemail" into input field having xpath "//*[@id='ap_email']"
    And I click on element having xpath "//*[@id='continue']"
    Then element having xpath "//*[@id='auth-email-invalid-email-alert']/div/div" should be present
    And element having xpath "//*[@id='auth-email-invalid-email-alert']/div/div" should have text as "Enter a valid email address"
    And element having xpath "//*[@id='auth-password-missing-alert']/div/div" should be present
    And element having xpath "//*[@id='auth-password-missing-alert']/div/div" should have text as "Enter your password"

  Scenario: Submit invalid password
    Given I navigate to "https://www.amazon.com/"
    When I click on element having xpath "//*[@id='nav-link-accountList']"
    And I click on element having xpath "//*[@id='createAccountSubmit']"
    And I enter "name" into input field having xpath "//*[@id='ap_customer_name']"
    And I enter "nahdu@gmail.com" into input field having xpath "//*[@id='ap_email']"
    And I enter "pass" into input field having xpath "//*[@id='ap_password']"
    And I click on element having xpath "//*[@id='continue']"
    Then element having xpath "//*[@id='auth-password-invalid-password-alert']/div/div" should be present
    And element having xpath "//*[@id='auth-password-invalid-password-alert']/div/div" should have text as "Password must be at least 6 characters."
    And element having xpath "//*[@id='auth-passwordCheck-missing-alert']/div/div" should be present
    And element having xpath "//*[@id='auth-passwordCheck-missing-alert']/div/div" should have text as "Type your password again"

  Scenario: Submit password not matching re-enter password
    Given I navigate to "https://www.amazon.com/"
    When I click on element having xpath "//*[@id='nav-link-accountList']"
    And I click on element having xpath "//*[@id='createAccountSubmit']"
    And I enter "name" into input field having xpath "//*[@id='ap_customer_name']"
    And I enter "nahdu@gmail.com" into input field having xpath "//*[@id='ap_email']"
    And I enter "password" into input field having xpath "//*[@id='ap_password']"
    And I enter "pass" into input field having xpath "//*[@id='ap_password_check']"
    And I click on element having xpath "//*[@id='continue']"
    Then element having xpath "//*[@id='auth-password-mismatch-alert']/div/div" should be present
    And element having xpath "//*[@id='auth-password-mismatch-alert']/div/div" should have text as "Password must match"

  Scenario: Submit valid datta
    Given I navigate to "https://www.amazon.com/"
    When I click on element having xpath "//*[@id='nav-link-accountList']"
    And I click on element having xpath "//*[@id='createAccountSubmit']"
    And I enter "aim" into input field having xpath "//*[@id='ap_customer_name']"
    And I enter "answermeloki@gmail.com" into input field having xpath "//*[@id='ap_email']"
    And I enter "password" into input field having xpath "//*[@id='ap_password']"
    And I enter "password" into input field having xpath "//*[@id='ap_password_check']"
    And I click on element having xpath "//*[@id='continue']"
    And I enter "757851" into input field having xpath "//*[@id='cvf-page-content']/div/div/div[1]/form/div[2]/input"
    And I click on element having xpath "//*[@id='a-autoid-0']/span/input"
    And I wait for 30 sec
    Then element having xpath "//*[@id='nav-link-accountList']/span[1]" should have text as "Hello, aim"
