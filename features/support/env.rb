require 'rubygems'
require 'selenium-cucumber'

# Store command line arguments
$browser_type = ENV['BROWSER'] || 'chrome'
$platform = ENV['PLATFORM'] || 'desktop'
$os_version = ENV['OS_VERSION']
$device_name = ENV['DEVICE_NAME']
$udid = ENV['UDID']
$app_path = ENV['APP_PATH']

# check for valid parameters
validate_parameters $platform, $browser_type, $app_path

# If platform is android or ios create driver instance for mobile browser
if $platform == 'android' or $platform == 'iOS'
  
  if $browser_type == 'native'
    $browser_type = "Browser"
  end
  
  if $platform == 'android'
    $device_name, $os_version = get_device_info
  end
  
  desired_caps = {
    caps:       {
      platformName:  $platform,
      browserName: $browser_type,
      versionNumber: $os_version,
      deviceName: $device_name,
      udid: $udid,
      app: ".//#{$app_path}"
      },
    }

  begin
    $driver = Appium::Driver.new(desired_caps).start_driver
  rescue Exception => e
    puts e.message
    Process.exit(0)  
  end
else # else create driver instance for desktop browser
  begin
    $driver = Selenium::WebDriver.for(:"#{$browser_type}")

    $driver.manage().window().maximize()
    # $driver.get("https://www.amazon.com")
    # $driver.manage.add_cookie(name: "session-id", value: "132-6138063-8061821")
    # $driver.manage.add_cookie(name: "session-token", value: "ELS9JKOVycDUW1aTXW7vWB+mO9w7VPfmms/l61yAKaczpQ9duRgwPeyhEMG2olxQyWjr0C23G1jqXC4SgAFWH4ygFdKEr13CS7alhEuchd6GctBwuFS4HAF5kyPVYiDxh1I8lVZd8DRk9W2ccnicOBJEbJpAJRqCoL2jbmCKXs95wETXy+mV6WT3VjJL84eP2YixBS8qiWOeVvE39Mo5RivodSnKUz5m3aq9Ins290Ad/EC2k1fIicGNTK55YBGDoAm/0mWyXNlY3+9JQTWEJCCeCWnhw8qM")
    # $driver.navigate.refresh()
  rescue Exception => e
    puts e.message
    Process.exit(0)
  end
end
