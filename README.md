HOW TO USE

Before running this project make sure:
1. Update your ruby to the lastest stabel version (this project use ruby 2.6.3p62)
2. Make use cucumber is installed. 
3. make sure you have downloaded chrome webdriver
4. make sure you have bundle installed.

To run this project use this code:

    To install all the dependencies use
        `bundle install`
        
    To run simply use
        `cucumber`
    
    If you wanna run specific feature use
        `cucumber path/to/file.feature`
        

HOW IT WORKS

This project will run the features with the static data given in this project.

The sign in feature will accsionally gets a hiccup because Amazon has defense mechanism against automation.
If this mechanism doesn't occur during the project run then the project will be working just fine.
Since Amazon send an OTP every time sign in happened, it is urged to use your own amazon account by providing your email and password in sign_in.feature file

The data in sign up feature will need to be updated as well because obviuosly the data provided already registered when the project is being developed
The data that need to be updated is in the scenario Submit valid datta

Good luck!